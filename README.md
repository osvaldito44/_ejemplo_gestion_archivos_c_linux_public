# _learn_C_

## [1.- /\_example_args_000\_/](https://bitbucket.org/fher_m5/_ejemplo_gestion_archivos_c_linux_public/src/master/_example_args_000_/ "_example_args_000_") ##

### Problema ###

![Imagen de ejemplo](_imgs_/_example_args_img_00.png "_example_args_img_00")
![Imagen de ejemplo](_imgs_/_example_args_img_01.png "_example_args_img_01")

### Ejecución ###

* Esto es un ejemplo de la ejecución del programa:
* Nota: Si se desea mostrar mensajes de Debug, poner a "1" la macro "_DEBUG" del archivo "config.h" y volver a compilar con "make"
```
embedded@embedded-VB ~/_docs_Fher_/_ejercicio_C_ $ ls -lrat
total 48
drwxrwxr-x 5 embedded embedded  4096 ene 17 14:10 ..
-rw-r--r-- 1 embedded embedded   176 ene 17 15:05 entrada.txt
-rw-r--r-- 1 embedded embedded 11660 ene 17 15:05 ejercicio.c
-rw-r--r-- 1 embedded embedded   161 ene 17 15:05 main.c
-rw-r--r-- 1 embedded embedded  1176 ene 17 15:05 ejercicio.h
-rw-r--r-- 1 embedded embedded   216 ene 17 15:05 Makefile
-rw-r--r-- 1 embedded embedded    83 ene 17 15:05 config.h
-rw-r--r-- 1 embedded embedded  1238 ene 17 15:05 glthread.h
drwxr-xr-x 2 embedded embedded  4096 ene 17 15:05 .
-rw-r--r-- 1 embedded embedded  1963 ene 17 15:05 glthread.c

embedded@embedded-VB ~/_docs_Fher_/_ejercicio_C_ $ make
gcc -g -c -Wall main.c -o main.o
gcc -g -c -Wall glthread.c -o glthread.o
gcc -g -c -Wall ejercicio.c -o ejercicio.o
gcc -g -Wall main.o glthread.o ejercicio.o -o application

embedded@embedded-VB ~/_docs_Fher_/_ejercicio_C_ $ nano entrada.txt 

embedded@embedded-VB ~/_docs_Fher_/_ejercicio_C_ $ more entrada.txt 
Me entere que estaban buscando personal a traves del grupo embedidos32.
Actualmente me encuentro laborando en la modalidad freelancer por proyectos, claro esto no es algo fijo.

embedded@embedded-VB ~/_docs_Fher_/_ejercicio_C_ $ ./application entrada.txt salida.dat
Desde el programa -->./application, file_src -->entrada.txt, file_dst -->salida.dat
argumento[0] = ./application
argumento[1] = entrada.txt
argumento[2] = salida.dat
Total de elementos _file_src [27]

<<<< Palabras en la Lista Enlazada >>>>
\```````````````````````````````````````````````````
palabra = traves
palabra = que
palabra = proyectos,
palabra = por
palabra = personal
palabra = no
palabra = modalidad
palabra = me
palabra = Me
palabra = laborando
palabra = la
palabra = grupo
palabra = freelancer
palabra = fijo.
palabra = esto
palabra = estaban
palabra = es
palabra = entere
palabra = encuentro
palabra = en
palabra = embedidos32.
palabra = del
palabra = claro
palabra = buscando
palabra = algo
palabra = Actualmente
palabra = a
\```````````````````````````````````````````````````
 ---> Nota: Estas mismas palabras deben estar en el archivo [salida.dat]


 --->Fin del programa...

embedded@embedded-VB ~/_docs_Fher_/_ejercicio_C_ $ more salida.dat 
a
Actualmente
algo
buscando
claro
del
embedidos32.
en
encuentro
entere
es
estaban
esto
fijo.
freelancer
grupo
la
laborando
Me
me
modalidad
no
personal
por
proyectos,
que
traves

embedded@embedded-VB ~/_docs_Fher_/_ejercicio_C_ $
```
* 
